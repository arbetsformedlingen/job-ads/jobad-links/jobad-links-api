from flask import Flask
from flask_cors import CORS
from loguru import logger as log

from joblinks import appconf, settings
from joblinks.rest.rest import api
from joblinks.rest.endpoint.api import SearchJobLink  # looks unneeded but is required

app = Flask(__name__, static_url_path='')
CORS(app)
log.info("Starting %s" % __name__)

log.info(f"Using Elasticsearch node at {settings.ES_HOST}:{settings.ES_PORT}")
log.info(f"Using Elasticsearch index/alias {settings.ES_INDEX}")
log.info(f"Using synonym dictionary loaded from endpoint: {settings.JAE_API_URL}/synonymdictionary")

if __name__ == '__main__':
    appconf.initialize_app(app, api)
    app.run(debug=True)
else:
    appconf.initialize_app(app, api)
