import json
from loguru import logger as log
from joblinks.repository.repository import elastic as elastic_client

from joblinks import settings
from joblinks.repository import fields
from joblinks.utils.decorators import singleton


@singleton
class AdsLocations(object):

    def __init__(self):
        self.client = elastic_client
        self.ads_index = settings.ES_INDEX
        self.extracted_locations = self._load_locations()

    def get_extracted_locations(self):
        return self.extracted_locations

    def _load_locations(self) -> set:
        '''
        Loads locations from the ads in Elastic.
        :return: A set with locations/cities from scraped structured ad data.
        '''
        query = {
            "aggs": {
                "ext_locations": {
                    "terms": {
                        "field": "%s.location.raw" % fields.KEYWORDS_EXTRACTED,
                        "size": 20000
                    }
                }
            },
            "query": {
                "bool": {
                    "must": [],
                    "filter": [
                        {
                            "range": {
                                fields.PUBLICATION_DATE: {
                                    "lte": "now/m+2H/m"
                                }
                            }
                        },
                        {
                            "range": {
                                fields.LAST_PUBLICATION_DATE: {
                                    "gte": "now/m+2H/m"
                                }
                            }
                        },
                        {
                            "term": {
                                fields.REMOVED: False
                            }
                        },
                    ],
                }
            },
            "size": 0
        }
        log.info(f"AdsLocations(_load_locations). Index: {self.ads_index} Query: {json.dumps(query)}")
        results = self.client.search(body=query, index=self.ads_index)
        ext_buckets = results.get('aggregations', {}).get('ext_locations', {}).get('buckets', [])
        found_locations = [p['key'] for p in ext_buckets if not p['key'].isnumeric()]
        return set(found_locations)

