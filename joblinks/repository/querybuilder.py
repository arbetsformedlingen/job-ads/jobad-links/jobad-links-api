import re
from datetime import datetime, timedelta
from dateutil import parser
from loguru import logger as log

from joblinks import settings
from joblinks.repository.text_to_concept import TextToConcept
from joblinks.repository import fields as query_fields
from joblinks.repository.querybuilder_freetext import QueryBuilderFreetext
from joblinks.repository.querybuilder_occupation import QueryBuilderOccupation
from joblinks.repository.querybuilder_location import QueryBuilderLocation



class QueryBuilder(object):
    def __init__(
        self,
        text_to_concept=TextToConcept(),
    ):
        self.text_to_concept = text_to_concept
        self.querybuilder_freetext = QueryBuilderFreetext(self.text_to_concept)
        self.querybuilder_occupation = QueryBuilderOccupation()
        self.querybuilder_location = QueryBuilderLocation()

    def parse_args(self, query_keywords: dict):

        query_dsl = self._bootstrap_query(query_keywords)
        must_queries = list()

        must_queries.append(
            self.querybuilder_freetext.build_freetext_query(
                query_keywords.get(settings.FREETEXT_QUERY),
                query_keywords.get(settings.FREETEXT_FIELDS),
            )
        )
        must_queries.append(
            self.querybuilder_occupation.build_occupation_query(
                query_keywords.get(settings.OCCUPATION),
                query_keywords.get(settings.GROUP),
                query_keywords.get(settings.FIELD),
            )
        )
        must_queries.append(
            self.querybuilder_location.build_location_query(
                query_keywords.get(settings.MUNICIPALITY),
                query_keywords.get(settings.REGION),
                query_keywords.get(settings.COUNTRY),
            )
        )

        must_queries.append(
            self._filter_timeframe(query_keywords.get(settings.PUBLISHED_AFTER))
        )
        must_queries.append(
            self._build_negative_source_filter_query(
                query_keywords.get(settings.EXCLUDE_SOURCE)
            )
        )

        query_dsl = self._assemble_queries(query_dsl, must_queries)

        return query_dsl

    @staticmethod
    def _bootstrap_query(query_keywords: dict):
        query_dsl = dict()
        query_dsl["from"] = query_keywords.pop(settings.OFFSET, 0)
        query_dsl["size"] = query_keywords.pop(settings.LIMIT, 10)
        query_dsl["query"] = {
            "bool": {"must": []},
        }
        query_dsl["track_total_hits"] = True
        query_dsl["track_scores"] = True

        if (
            query_keywords.get(settings.SORT)
            and query_keywords.get(settings.SORT) in query_fields.sort_options.keys()
        ):
            query_dsl["sort"] = query_fields.sort_options.get(
                query_keywords.pop(settings.SORT)
            )
        else:
            query_dsl["sort"] = query_fields.sort_options.get("relevance")

        return query_dsl

    @staticmethod
    def _assemble_queries(query_dsl, additional_queries):
        for query in additional_queries:
            if query:
                query_dsl["query"]["bool"]["must"].append(
                    query
                )

        return query_dsl

    @staticmethod
    def _rewrite_word_for_regex(word: str) -> str:
        """Inserts a \ in front of the characters in bad_chards"""
        if word is None:
            word = ""

        bad_chars = [
            "+",
            ".",
            "[",
            "]",
            "{",
            "}",
            "(",
            ")",
            "^",
            "$",
            "*",
            "\\",
            "|",
            "?",
            '"',
            "'",
            "&",
            "<",
            ">",
        ]

        if any(c in bad_chars for c in word):
            modded_term = ""
            for c in word:
                if c in bad_chars:
                    modded_term += "\\"
                modded_term += c
            return modded_term

        return word

    def _rewrite_querystring(self, querystring, concepts):
        # Sort all concepts by string length
        all_concepts = sorted(
            concepts.get("occupation", [])
            + concepts.get("occupation_must", [])
            + concepts.get("occupation_must_not", [])
            + concepts.get("skill", [])
            + concepts.get("skill_must", [])
            + concepts.get("skill_must_not", [])
            + concepts.get("location", [])
            + concepts.get("location_must", [])
            + concepts.get("location_must_not", []),
            key=lambda c: len(c),
            reverse=True,
        )

        # Remove found concepts from querystring
        for term in [concept["term"] for concept in all_concepts]:
            term = self._rewrite_word_for_regex(term)
            p = re.compile(f"(^|\\s+)(\\+{term}|\\-{term}|{term})(\\s+|$)")
            querystring = p.sub("\\1\\3", querystring).strip()

        # Remove duplicate spaces
        querystring = re.sub("\\s+", " ", querystring).strip()

        return querystring

    @staticmethod
    def _build_negative_source_filter_query(excluded_source):
        if not excluded_source:
            return None

        query = {
            "bool": {
                "must_not": {
                    "nested": {
                        "path": "source_links",
                        "query": {
                            "bool": {
                                "must": [
                                    {"term": {"source_links.label": excluded_source}}
                                ]
                            }
                        },
                    }
                }
            }
        }

        return query

    @staticmethod
    def _filter_timeframe(from_datestring):
        if not from_datestring:
            return None

        range_query = {"range": {query_fields.PUBLICATION_DATE: {}}}
        from_datetime = None

        if from_datestring and re.match(r"^\d+$", from_datestring):
            now = datetime.now()
            from_datetime = now - timedelta(minutes=int(from_datestring))
        elif from_datestring:
            from_datetime = parser.parse(from_datestring)

        if from_datetime:
            log.debug("Filter ads from %s" % from_datetime)
            range_query["range"][query_fields.PUBLICATION_DATE][
                "gte"
            ] = from_datetime.isoformat()

        return range_query
