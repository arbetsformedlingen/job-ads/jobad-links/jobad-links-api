from joblinks.repository import fields as query_fields


class QueryBuilderOccupation:
    def __init__(self):
        pass

    # Parses OCCUPATION, FIELD and GROUP
    def build_occupation_query(
        self, occupation_roles, occupation_groups, occupation_areas
    ):
        """Builds the occupation terms in the query"""
        occupations = occupation_roles or []
        occupation_groups = occupation_groups or []
        occupation_areas = occupation_areas or []

        # ------- must query
        should_occupation_term_query = [
            {
                "term": {
                    query_fields.OCCUPATION
                    + "."
                    + query_fields.CONCEPT_ID
                    + ".keyword": {"value": y, "boost": 2.0}
                }
            }
            for y in occupations
            if y and not y.startswith("-")
        ]

        should_occupation_term_query += [
            {
                "term": {
                    query_fields.OCCUPATION_GROUP
                    + "."
                    + query_fields.CONCEPT_ID
                    + ".keyword": {"value": y, "boost": 1.0}
                }
            }
            for y in occupation_groups
            if y and not y.startswith("-")
        ]

        should_occupation_term_query += [
            {
                "term": {
                    query_fields.OCCUPATION_FIELD
                    + "."
                    + query_fields.CONCEPT_ID
                    + ".keyword": {"value": y, "boost": 1.0}
                }
            }
            for y in occupation_areas
            if y and not y.startswith("-")
        ]

        # ------- must not query
        mustnot_occupation_term_query = [
            {
                "term": {
                    query_fields.OCCUPATION
                    + "."
                    + query_fields.CONCEPT_ID
                    + ".keyword": {"value": y[1:]}
                }
            }
            for y in occupations
            if y and y.startswith("-")
        ]

        mustnot_occupation_term_query += [
            {
                "term": {
                    query_fields.OCCUPATION_GROUP
                    + "."
                    + query_fields.CONCEPT_ID
                    + ".keyword": {"value": y[1:]}
                }
            }
            for y in occupation_groups
            if y and y.startswith("-")
        ]

        mustnot_occupation_term_query += [
            {
                "term": {
                    query_fields.OCCUPATION_FIELD
                    + "."
                    + query_fields.CONCEPT_ID
                    + ".keyword": {"value": y[1:]}
                }
            }
            for y in occupation_areas
            if y and y.startswith("-")
        ]

        # -----------------------
        if should_occupation_term_query or mustnot_occupation_term_query:
            query = {"bool": {}}
            if should_occupation_term_query:
                query["bool"]["should"] = should_occupation_term_query

            if mustnot_occupation_term_query:
                query["bool"]["must_not"] = mustnot_occupation_term_query

            return query
        else:
            return None
