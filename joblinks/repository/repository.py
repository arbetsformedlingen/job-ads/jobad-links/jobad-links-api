import certifi
from ssl import create_default_context
from loguru import logger as log

from elasticsearch import Elasticsearch
from joblinks import settings

log.info(f"Using Elasticsearch node at: {', '.join(settings.ES_HOST)} (port {settings.ES_PORT})")
if settings.ES_USER and settings.ES_PWD:
    context = create_default_context(cafile=certifi.where())
    elastic = Elasticsearch([
        {
            "host": host,
            "port": settings.ES_PORT,
            "use_ssl": True,
            "scheme": "https",
            "ssl_context": context,
            "http_auth": (settings.ES_USER, settings.ES_PWD)
        }
        for host in settings.ES_HOST
    ])
else:
    elastic = Elasticsearch([
        {
            "host": host,
            "port": settings.ES_PORT
        }
        for host in settings.ES_HOST
    ])
