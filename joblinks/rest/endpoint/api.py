from loguru import logger as log
from flask_restx import Resource
from joblinks import settings
from joblinks.repository.querybuilder import QueryBuilder
from joblinks.rest import fetchfunction
from joblinks.rest.rest import load_jl_ad_query
from joblinks.rest.rest import ns_joblinks, jl_query
from joblinks.rest.model import apis


PUBLISHED_AFTER = "published-after"
GROUP = "occupation-group"
FIELD = "occupation-field"
MUNICIPALITY = "municipality"
REGION = "region"
COUNTRY = "country"
QUERY = "q"
LIMIT = "limit"
OFFSET = "offset"
SORT = "sort"
EXCLUDE_SOURCE = "exclude_source"


@ns_joblinks.route("joblinks")
class SearchJobLink(Resource):
    @ns_joblinks.doc(
        description="Search scraped ads using parameters and/or freetext\n",
        params={
            PUBLISHED_AFTER: "Fetch job ads published after specified date and time.",
            GROUP: "One or more occupational group concept ID according to the taxonomy",
            FIELD: "One or more occupational field concept ID according to the taxonomy",
            MUNICIPALITY: "One or more municipality concept ID according to the taxonomy",
            REGION: "One or more region concept ID according to the taxonomy",
            COUNTRY: "One or more country concept ID according to the taxonomy",
            SORT: "Sorting. "
            "relevance: relevance (points) (default sorting)"
            "pubdate-desc: published date, descending (newest job ad first)"
            "pubdate-asc: published date, ascending (oldest job ad first)",
            QUERY: "Freetext query",
            OFFSET: "The offset parameter defines the offset "
            "from the first result you want to fetch. Valid range: (0-%d)"
            % settings.MAX_OFFSET,
            LIMIT: "Number of results to fetch. Valid range: (0-%d)"
            % settings.MAX_LIMIT,
            EXCLUDE_SOURCE: "A source (i.e. source_links label) to exclude from result",
        },
    )

    @ns_joblinks.expect(jl_query)
    def get(self, **kwargs):
        querybuilder = QueryBuilder()
        args = jl_query.parse_args()
        result = fetchfunction.find_all(args, querybuilder)
        log.info(f"ARGS: {args}")
        log.debug(f"Result: {result}")
        max_score = result.get("max_score", 1.0)
        hits = [
            dict(
                hit["_source"],
                **{
                    "relevance": (hit["_score"] / max_score)
                    if max_score and max_score > 0
                    else 0.0
                },
            )
            for hit in result.get("hits", [])
        ]
        return self.marshal_results(result, hits)

    def marshal_results(self, esresult, hits):
        total_results = {"value": esresult.get("total", {}).get("value")}
        result = {"total": total_results, "hits": self.convert_hits(hits)}
        return result

    @staticmethod
    def convert_hits(hits):
        result = []
        for hit in hits:
            result.append(format_single_result(hit))
        return result


@ns_joblinks.route("ad/<id>", endpoint="ad")
class Proxy(Resource):
    @ns_joblinks.doc(
        description="Load a job ad by ID",
    )
    @ns_joblinks.response(404, "Job ad not found")
    @ns_joblinks.expect(load_jl_ad_query)
    @ns_joblinks.marshal_with(apis.job_ad)
    def get(self, id, *args, **kwargs):
        result = fetchfunction.find_ad_by_id(id)
        return self.marshal_results(result)

    @staticmethod
    def marshal_results(esresult):
        return format_single_result(esresult)


def format_single_result(esresult):
    # TODO Temporary to avoid problems if api is used with an index that does not have new field
    publication_date = esresult.get("date_to_display_as_publish_date", None)
    if not publication_date:
        publication_date = esresult["firstSeen"]

    result = {
        "id": str(esresult.get("id", "")),
        "headline": esresult.get("originalJobPosting", {}).get("title", ""),
        "brief": esresult.get("originalJobPosting", {}).get("brief", ""),
        "occupation_group": esresult.get("occupation_group", {}),
        "occupation_field": esresult.get("occupation_field", {}),
        "employer": esresult.get("originalJobPosting", {}).get("employer", ""),
        "workplace_addresses": esresult.get("workplace_addresses", []),
        "publication_date": publication_date,
        "source_links": esresult.get("source_links", []),
    }
    return result
