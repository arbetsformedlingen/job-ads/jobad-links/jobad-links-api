import json
import time

from elasticsearch import exceptions
from flask_restx import Namespace, abort
from loguru import logger as log

from joblinks import settings
from joblinks.repository.repository import elastic

marshaller = Namespace('Marshaller')


def find_all(args, querybuilder, start_time=0):
    if start_time == 0:
        start_time = int(time.time() * 1000)
    query_dsl = querybuilder.parse_args(args)
    log.info('QUERY: %s' % json.dumps(query_dsl))
    log.debug("Query constructed after %d milliseconds." % (int(time.time() * 1000) - start_time))
    try:
        query_result = elastic.search(index=settings.ES_INDEX, body=query_dsl)
        log.debug(f"Elastic results after {(int(time.time() * 1000) - start_time)} milliseconds: \n {query_result}")
    except exceptions.ConnectionError as e:
        log.exception(f'Failed to connect to elasticsearch: {e}')
        abort(500, 'Failed to establish connection to database')
        return

    log.debug("Elasticsearch reports: took=%d, timed_out=%s"
              % (query_result.get('took', 0), query_result.get('timed_out', '')))
    return transform_platsannons_query_result(query_result)


def transform_platsannons_query_result(query_result):
    results = query_result.get('hits', {})
    results['took'] = query_result.get('took', 0)
    results['concepts'] = query_result.get('concepts', {})
    return results


def find_ad_by_id(ad_id: str, start_time: int = 0) -> dict:
    if start_time == 0:
        start_time = int(time.time() * 1000)

    if not ad_id:
        abort(404, 'No id provided')

    try:
        query_result: dict = elastic.get(index=settings.ES_INDEX, id=ad_id, ignore=404)
        log.debug("Elastic results after %d milliseconds." % (int(time.time() * 1000) - start_time))
    except exceptions.ConnectionError as e:
        log.exception(f'Failed to connect to elasticsearch: {e}')
        abort(500, 'Failed to establish connection to database')
    except exceptions.RequestError as re:
        log.exception(f"Exception {re} for id {ad_id}")
        abort(404, 'Ad not found')

    if not query_result.get('found', False):
        abort(404, 'Ad not found')

    return query_result.get('_source')
