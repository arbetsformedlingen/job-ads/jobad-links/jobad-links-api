from flask_restx import fields
from joblinks.rest.rest import api as root_api


id = root_api.model("Id", {"Id": fields.String()})
number_of_hits = root_api.model("NumberOfHits", {"value": fields.Integer()})
headline = root_api.model(
    "Headline",
    {
        "Headline": fields.String(),
    },
)
brief = root_api.model("Headline", {"Value": fields.String()})
occupation_group = root_api.model(
    "OccupationGroup", {"label": fields.String(), "concept_id": fields.String()}
)

occupation_field = root_api.model(
    "OccupationField", {"label": fields.String(), "concept_id": fields.String()}
)

employer = root_api.model("Employer", {"name": fields.String()})

workplace_addresses = root_api.model(
    "WorkplaceAddresses", {"municipality_concept_id": fields.String(),
                        "municipality": fields.String(),
                        "region_concept_id": fields.String(),
                        "region": fields.String(),
                        "country_concept_id": fields.String(),
                        "country": fields.String()
    }
)

publication_date = root_api.model("PublicationDate", {"Value": fields.String()})

source_links = root_api.model(
    "SourceLinks", {"label": fields.String(), "url": fields.String()}
)

job_ad = root_api.model(
    "JobAd",
    {
        "id": fields.String(),
        "headline": fields.String(),
        "brief": fields.String(),
        "occupation_group": fields.Nested(occupation_group),
        "occupation_field": fields.Nested(occupation_field),
        "employer": fields.Nested(employer),
        "workplace_addresses": fields.Nested(workplace_addresses),
        "publication_date": fields.String(),
        "source_links": fields.List(fields.Nested(source_links)),
    },
)
