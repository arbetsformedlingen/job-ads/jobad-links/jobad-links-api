from flask_restx import Api, Namespace, reqparse, inputs
from joblinks import settings

# QUERYFIELDS
ID = 'id'
PUBLICATION_DATE = 'firstSeen'

sort_options = {
    'relevance': ["_score", {PUBLICATION_DATE: "desc"}],
    'pubdate-desc': [{PUBLICATION_DATE: "desc"}, "_score", {ID: "asc"}],
    'pubdate-asc': [{PUBLICATION_DATE: "asc"}, "_score", {ID: "asc"}],
}


def lowercase_max_length(value):
    if not value:
        raise ValueError('string type must be non-null')
    if len(value) > 255:
        raise ValueError('parameter can not be longer than 255 characters')

    return str(value).lower()


api = Api(version=settings.API_VERSION, title='JobAd Links',
          description="""This API is a joint effort between The Swedish Public Employment Service and some of Sweden's largest job-board sites. The dataset that is searchable in this API is named JobAd Links and contains references and metadata linked to the job-ads provided by job-boards. Around 30% more ads than Platsbanken.

  1. Free to use for everyone

  2. No registration is needed

  3. [Contact us](mailto:jobtechdev@arbetsformedlingen.se) if you want to enquire more than 1000 requests / day

  4. If you are a analyst you can get the raw data as files [here](https://data.jobtechdev.se/annonser/jobtechlinks)

# How it works

The ads shown in the API is updated once a day. This service are using machine learning to classify ads according to the swedish classification system for occupations (SSYK). Every job ad description is truncated to enforce that the end user visits the origin job site. We want users of the API to respect the business model to link back to the origin site.

# Getting started

A getting started guide for search queries is [documented here](https://gitlab.com/arbetsformedlingen/job-ads/joblinks-search-api/-/blob/main/docs/gettingStarted.md). Easiest is to free text search with the attribute **q** below, example 'systemutvecklare stockholm' etc. If you want to search for official occupations based on SSYK, use [this end user interface for occasional lookup](https://atlas.jobtechdev.se/) or [the taxononomy API for programmatic access](https://taxonomy.api.jobtechdev.se/v1/taxonomy/swagger-ui/) for getting the ids for occupation-groups and -fields. For example the occupation-group 'Bagare och konditorer' translates to Concept-id '5qT8_z9d_8rw' that can be used in the field occupation-group.

## Useful links

  - [Getting Started](https://gitlab.com/arbetsformedlingen/job-ads/joblinks-search-api/-/blob/main/docs/gettingStarted.md)

  - [Code examples](https://gitlab.com/arbetsformedlingen/job-ads/getting-started-code-examples/code-examples-start-here)

  - [Taxonomy Getting Started](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/blob/develop/GETTINGSTARTED.md)

  - [Taxonomy Atlas](https://atlas.jobtechdev.se)
  
  - [JobTech Forum](https://forum.jobtechdev.se/c/vara-api-er-dataset/jobad-links/40)

""",
          default='JobAd Links',
          default_label="An API for searching scraped ads")

ns_joblinks = Namespace('JobAd Links', description='Endpoint for JobAd Links')
api.add_namespace(ns_joblinks, '/')

datetime_or_minutes_regex = r'^(\d\d\d\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01])T(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9]))|(\d+)$'
jl_query = reqparse.RequestParser()
jl_query.add_argument(settings.PUBLISHED_AFTER, type=inputs.regex(datetime_or_minutes_regex))
jl_query.add_argument(settings.GROUP, action='append')
jl_query.add_argument(settings.FIELD, action='append')
jl_query.add_argument(settings.MUNICIPALITY, action='append')
jl_query.add_argument(settings.REGION, action='append')
jl_query.add_argument(settings.COUNTRY, action='append')
jl_query.add_argument(settings.SORT, choices=list(sort_options.keys()))
jl_query.add_argument(settings.QUERY, type=lowercase_max_length)
jl_query.add_argument(settings.OFFSET, type=inputs.int_range(0, settings.MAX_OFFSET), default=0)
jl_query.add_argument(settings.LIMIT, type=inputs.int_range(0, settings.MAX_LIMIT), default=10)
jl_query.add_argument(settings.EXCLUDE_SOURCE)

load_jl_ad_query = reqparse.RequestParser()
