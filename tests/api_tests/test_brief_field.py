import pytest
from tests.test_resources.helper import get_search, check_number_of_ads


@pytest.mark.parametrize("query, expected",
                         ([("universitetslektor", 143),
                           ("skogsarbetare", 16),
                           ("sjuksköterska", 6167)]))
def test_brief(query, expected):
    """
    Check that the 'brief' section exists and has some text
    """
    json_response = get_search(params={'q': query, 'limit': '100'})
    check_number_of_ads(json_response, expected)
    hits = json_response['hits']
    for hit in hits:
        assert len(hit['brief']) > 10
