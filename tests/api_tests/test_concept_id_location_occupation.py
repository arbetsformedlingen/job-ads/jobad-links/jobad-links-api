import pytest
from tests.test_resources.helper import check_number_of_ads, get_search
from tests.test_resources.concept_ids import concept_ids_geo as geo
from tests.test_resources.concept_ids import occupation_group as group
from tests.test_resources.concept_ids import occupation_field as field


@pytest.mark.parametrize("params, expected", [
    ({'occupation-group': group.arbetsformedlare}, 529),
    ({'occupation-group': group.bagare_och_konditorer}, 46),
    ({'occupation-group': [group.kockar_och_kallskankor, group.anlaggningsmaskinforare_m_fl_]}, 2045),
    ({'occupation-group': [group.kockar_och_kallskankor, group.anlaggningsmaskinforare_m_fl_],
      'region': geo.jamtlands_lan}, 12),
    ({'occupation-field': field.transport}, 5810),
    ({'occupation-field': field.sanering_och_renhallning}, 2025),
    ({'occupation-field': [field.transport, field.sanering_och_renhallning]}, 7835),
    ({'occupation-field': [field.transport, field.sanering_och_renhallning],
      'region': [geo.stockholms_lan, geo.ostergotlands_lan]}, 1757)
])
def test_occupation_group_and_fields( params, expected):
    check_number_of_ads(get_search(params), expected)
