import pytest
from tests.test_resources.helper import get_search

search_terms = ['barnmorska',
                'tandsköterska',
                'kock',
                'Sjuksköterskor',
                'Sjuksköterska',
                'Stockholm',
                'Göteborg',
                'Malmö',
                'utvecklare',
                'lärare kock',
                'Sollentuna',
                'Sollentuna Umeå',
                'Sjuksköterska Göteborg',
                'Helsingborg',
                'sjuksköterskor',
                'sjuksköterska',
                'Uppsala',
                'sjuksköterskor Uppsala',
                "python",
                "java",
                "java -javautvecklare",
                "java python",
                "java +python",
                "java -python",
                "kock -bagare",
                "pizzabagare",
                "bartender",
                "målare",
                "målare stockholm",
                "målare +stockholm",
                "målare -stockholm",
                "personlig assistent",
                "personlig assistent +göteborg",
                "personlig assistent -göteborg",
                "utvecklare",
                "förskollärare",
                "sjuksköterska",
                "sjuksköterska -stockholm", "C#",
                "c-körkort",
                ".net",
                "ci/cd",
                "erp-system",
                "tcp/ip",
                "cad-verktyg",
                "backend-utvecklare",
                "it-tekniker", 'obstetrix',
                'tracheostomi',
                'yrkesbevis',
                'körkort',
                'livsmedel',
                'livsmedel distributör',
                ' distributör', 'sköterska',
                'omsorgspersonal',
                'undersköterska',
                'undersköterska omsorgspersonal',
                'brandman', 'lärarutbildning',
                'datasystem',
                'undervisning',
                'its',
                'introduktionsprogram',
                'försäkring',
                'specialistutbildning',
                ]


@pytest.mark.skip("used for creating test data")
def test_query():
    """
    This test will take the list of search terms and do a call for each one of them and print the number of hits
    used to get an idea on how frequent the terms are in different versions of the input file
    """
    for query in search_terms:
        params = {'q': query, 'limit': '0'}
        json_response = get_search(params)
        print(json_response['total']['value'])
