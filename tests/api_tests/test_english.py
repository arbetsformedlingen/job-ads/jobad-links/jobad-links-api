import pytest
from tests.test_resources.helper import get_search

concept_ids = {
    "Hotell, restaurang, storhushåll": "ScKy_FHB_7wT",
    "Malmö": "oYPt_yRA_Smm",
    "Lager- och terminalpersonal": "kLyY_rwr_aJr",
    "Västerbottens län": "g5Tt_CAV_zBd",
}


@pytest.mark.parametrize("param, expected", [
    ({'q': 'english', 'occupation-field': concept_ids["Hotell, restaurang, storhushåll"]}, 81),
    ({'occupation-field': concept_ids["Hotell, restaurang, storhushåll"]}, 6330),
    ({'q': 'english', 'country': 'i46j_HmG_v64'}, 5790),
    ({'q': 'english', 'municipality': concept_ids['Malmö']}, 504),
    ({'municipality': concept_ids['Malmö']}, 3729),
    ({'region': concept_ids["Västerbottens län"]}, 2034),
    ({'q': 'english', 'region': concept_ids["Västerbottens län"]}, 190),
    ({'q': 'english', 'occupation-group': concept_ids["Lager- och terminalpersonal"]}, 14),
    ({'occupation-group': concept_ids["Lager- och terminalpersonal"]}, 3359),
    ({'q': 'skara english'}, 3),
    ({'q': 'stockholm english'}, 3036),
    ({'q': 'developer english'}, 1492),
    ({'q': 'göteborg english'}, 399),
    ({'q': 'gävle english'}, 13),
    ({'q': 'östersund english'}, 22),
    ({'q': 'java'}, 1495),
    ({'q': 'java english'}, 662),
    ({'q': 'python'}, 1606),
    ({'q': 'python english'}, 897),
    ({'q': 'english'}, 8715),
    ({'q': 'english teacher'}, 8739),
    ({'q': 'lärare english'}, 93),
    ({'q': 'stockholm english'}, 3036),
    ({'q': 'english speaking'}, 8730),
    ({'q': 'english speaking stockholm'}, 3038),
    ({'q': 'english speaking jobs'}, 8730),
    ({'q': 'english jobs'}, 8715),
    ({'q': 'nacka english'}, 34),
    ({'q': 'uppsala english'}, 205),
    ({'q': 'uppsala -english'}, 1654),
    ({'q': 'uppsala'}, 1657),
])
def test_english_and_params( param, expected):
    param.update({'limit': '0'})
    result = get_search(param)
    number_of_hits = result['total']['value']
    assert number_of_hits == expected, f"ERROR, {number_of_hits} hits for {param}, expected {expected}"
