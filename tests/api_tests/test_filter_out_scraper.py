import pytest
from tests.test_resources.helper import get_search, label_in_source_links
from tests.test_resources.sources import sources
from joblinks.settings import EXCLUDE_SOURCE


@pytest.mark.parametrize("source_to_exclude", sources)
def test_filter( source_to_exclude):
    query = 'målare stockholm'  # something that gives < 100 hits so that the test doesn't need offset
    number_of_hits_to_exclude = 0

    unfiltered_hits = get_search({'q': query, 'limit': 100})['hits']

    assert len(unfiltered_hits) > 0
    for hit in unfiltered_hits:
        if label_in_source_links(source_to_exclude, hit['source_links']):
            number_of_hits_to_exclude += 1

    filtered_hits = get_search({'q': query, 'limit': 100, EXCLUDE_SOURCE: source_to_exclude})['hits']
    assert len(filtered_hits) <= len(unfiltered_hits)
    print(
        f"source: {source_to_exclude} : unfiltered: {len(unfiltered_hits)}, filtered: {len(filtered_hits)}, hits to exclude: {number_of_hits_to_exclude} ")

    for hit in filtered_hits:
        assert not label_in_source_links(source_to_exclude, hit['source_links'])

    expected_number = (len(unfiltered_hits) - number_of_hits_to_exclude)
    assert len(filtered_hits) == expected_number


@pytest.mark.parametrize("query", ["C#", "c-körkort", ".net", "it-tekniker", 'sjuksköterska', ' stockholm', 'python'])
@pytest.mark.parametrize("source_to_exclude", sources)
def test_filter_simple( query, source_to_exclude):
    unfiltered = get_search({'q': query, 'limit': 0})['total']['value']
    filtered = get_search({'q': query, EXCLUDE_SOURCE: source_to_exclude, 'limit': 0})['total']['value']
    print(f"source: {source_to_exclude}, query: {query} unfiltered: {unfiltered}, filtered: {filtered}")
    assert unfiltered >= filtered


@pytest.mark.parametrize("wrong_param", [-1, 0, 1.2, None, [], {}, 'does_not_exist', f"{sources[0]}, {sources[1]}"])
def test_exclude_source_error( wrong_param):
    query = 'sjuksköterska'
    limit = 0
    unfiltered_number = get_search({'q': query, 'limit': limit})['total']['value']
    assert unfiltered_number > 0
    wrong_params = {'q': query, 'limit': limit, 'exclude_source': wrong_param}
    filtered_number = get_search(wrong_params)['total']['value']
    assert filtered_number == unfiltered_number
