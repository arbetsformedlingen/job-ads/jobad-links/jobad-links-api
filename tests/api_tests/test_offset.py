import json
import pytest
import requests
from tests.test_resources.helper import get_search, get_joblinks_raw


@pytest.mark.smoke
def test_offset_diff():
    last_result = []
    for offset in [100, 200, 500, 1200, 1999]:
        json_response = get_search(params={'offset': offset, 'limit': 100})
        current_result = json_response['hits']
        assert current_result != last_result
        last_result = current_result


@pytest.mark.parametrize("offset, wrong_offset",
                         [(100, False), (1000, False), (2000, False), (2001, True), (-1, True), (0, False),
                          (-2001, True)])
def test_wrong_offset( offset, wrong_offset):
    response = get_joblinks_raw(params={'offset': offset, 'limit': '0'})
    response_json = json.loads(response.content.decode('utf8'))
    if wrong_offset:
        assert response.status_code == requests.codes.bad_request
        assert "argument must be within the range 0 - 2000" in response_json['errors']['offset']
        assert 'Input payload validation failed' in str(response.text)
    else:
        response.raise_for_status()
