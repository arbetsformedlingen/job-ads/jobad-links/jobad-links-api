#  -*- coding: utf-8 -*-
import pytest
from tests.test_resources.test_cases import test_cases
from tests.test_resources.helper import get_total


@pytest.mark.parametrize("param, expected", test_cases)
def test_search( param, expected):
    number_of_hits = get_total(param)
    assert number_of_hits == expected, f"ERROR, {number_of_hits} hits for {param}, expected {expected}"
