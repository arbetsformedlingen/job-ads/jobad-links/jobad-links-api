from tests.test_resources.helper import verify_source_links


def test_source_links( get_10_random_ads):
    for ad in get_10_random_ads:
        verify_source_links(ad['source_links'])
