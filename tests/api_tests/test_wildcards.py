from http import HTTPStatus
import pytest
from tests.test_resources.helper import get_joblinks_raw


@pytest.mark.parametrize("query", [
    'abc*', '*abc', '*abck*', '*abc*xyz', 'abc*xyz*', '*abc*xyz*', '*', 'abc*xyz'
])
def test_wildcard( query):
    """
    Reproduce NAR-825
    """
    response = get_joblinks_raw(params={'q': query})
    assert response.status_code == HTTPStatus.OK
