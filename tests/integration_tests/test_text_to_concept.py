import pytest

from tests.integration_tests.test_query_rewrite import text_to_concept


@pytest.mark.integration
def test_clean_plus_minus():
    input_text = '-mållare målare +undersköterska java-utvecklare -key account manager c-sharp -java -noggrann flexibel'
    expected = 'mållare målare undersköterska java-utvecklare key account manager c-sharp java noggrann flexibel'
    cleaned_text = text_to_concept.clean_plus_minus(input_text)
    assert cleaned_text == expected


@pytest.mark.integration
@pytest.mark.parametrize("concept, concept_type, operator, expect_success", [
    ({'term': 'noggrann', 'uuid': '0311957f-56c2-5009-abd4-8800621b854c', 'concept': 'Noggrann', 'type': 'FORMAGA',
      'term_uuid': '4a510449-2bef-5e1c-8a9b-f3919d01cbec', 'term_misspelled': False,
      'build_datetime': '2020-08-11T10:14:56', 'build_datetime_unix_epoch': 1597133696, 'plural_occupation': False,
      'definite_occupation': False, 'version': 'SYNONYM-DIC-2.0.1.210', 'operator': ''}
     , 'KOMPETENS', '', False),
    ({'term': 'java', 'uuid': 'c965e8aa-751a-5923-97bd-b8bd6d5e813a', 'concept': 'Java', 'type': 'KOMPETENS',
      'term_uuid': 'e3d2a75a-5717-56d2-ad8a-ee4b5baf8530', 'term_misspelled': False,
      'build_datetime': '2020-08-11T10:14:56', 'build_datetime_unix_epoch': 1597133696,
      'plural_occupation': False, 'definite_occupation': False, 'version': 'SYNONYM-DIC-2.0.1.210',
      'operator': ''},
     'KOMPETENS', '', True),
    ({'term': 'systemutvecklare', 'uuid': 'df9e7a73-2cc3-5b32-a84e-7e68a527e80e', 'concept': 'Systemutvecklare',
      'type': 'YRKE', 'term_uuid': '7296755c-acf2-5eed-9d4b-e4cd845cd05a', 'term_misspelled': False,
      'build_datetime': '2020-08-11T10:14:56', 'build_datetime_unix_epoch': 1597133696, 'plural_occupation': False,
      'definite_occupation': False, 'version': 'SYNONYM-DIC-2.0.1.210', 'operator': ''},
     'KOMPETENS', '+', False),
    ({'term': 'noggrann', 'uuid': '0311957f-56c2-5009-abd4-8800621b854c', 'concept': 'Noggrann', 'type': 'FORMAGA',
      'term_uuid': '4a510449-2bef-5e1c-8a9b-f3919d01cbec', 'term_misspelled': False,
      'build_datetime': '2020-08-11T10:14:56', 'build_datetime_unix_epoch': 1597133696, 'plural_occupation': False,
      'definite_occupation': False, 'version': 'SYNONYM-DIC-2.0.1.210', 'operator': ''},
     'KOMPETENS', '-', False)
])
def test_filter_concepts(concept, concept_type, operator, expect_success):
    assert expect_success == text_to_concept.filter_concepts(concept, concept_type, operator)
