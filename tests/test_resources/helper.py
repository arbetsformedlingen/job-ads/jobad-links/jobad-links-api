import random
import json
from datetime import datetime, timedelta
import copy
import requests

from tests.test_resources.test_settings import TEST_URL


def random_ad_ids(hits):
    if len(hits) >= 10:
        max_number = 10
    else:
        max_number = len(hits)

    random_ads = random.sample(hits, random.randint(1, max_number))
    random_ids = []
    for ad in random_ads:
        random_ids.append(ad['id'])
    return random_ids


def get_ad_id_raw( ad_id):
    return requests.get(f"{TEST_URL}/ad/{ad_id}")


def get_by_id(ad_id):
    response = get_ad_id_raw(ad_id)
    response.raise_for_status()
    return json.loads(response.content.decode('utf8'))


def get_by_id_expect_status_code(ad_id, expected):
    response = get_ad_id_raw(ad_id)
    status_code = response.status_code
    assert status_code == expected, f"expected http status code {expected}, but got {status_code}"


def get_search(params):
    response = get_joblinks_raw(params)
    response.raise_for_status()
    return json.loads(response.content.decode('utf8'))


def get_joblinks_raw(params):
    return requests.get(f"{TEST_URL}/joblinks", params=params)


def get_total(params):
    params.update({'limit': 0})
    return get_search(params)['total']['value']


def check_required_ad_fields_not_none(hit):
    assert hit['id'] is not None
    assert hit['external_id'] is not None
    assert hit['webpage_url'] is not None
    assert hit['headline'] is not None
    assert hit['hashsum'] is not None
    assert len(hit) == 10


def check_ad_structure(ad):
    assert len(ad) == 9
    assert ad['id'] is not None
    assert ad['headline'] is not None
    assert ad['brief'] is not None
    workplace_address_list = ad['workplace_addresses']
    assert workplace_address_list is not None
    assert isinstance(workplace_address_list, list), f"expected a list but got a {type(workplace_address_list)}"
    if len(workplace_address_list) > 0:  # allow empty list # TODO stricter with static test data
        assert len(workplace_address_list[0]) == 6, f"Error: {workplace_address_list}"
        for item in workplace_address_list:
            assert isinstance(item, dict)
            for key in ["municipality_concept_id",
                        "municipality",
                        "region_concept_id",
                        "region",
                        "country_concept_id",
                        "country"]:
                assert item.get(key, None) is not None

    if occ_group := ad['occupation_group']:
        check_occupation_dict(occ_group)
    if occ_field := ad['occupation_field']:
        check_occupation_dict(occ_field)

    assert ad['publication_date'] is not None
    assert isinstance(ad['publication_date'], str)

    employer = ad.get("employer", None)
    assert employer is not None
    assert isinstance(employer, dict)
    assert employer["name"] is not None

    source_links = ad["source_links"]
    assert source_links is not None
    assert isinstance(source_links, list), f"expected a list but got a {type(source_links)}"
    for item in source_links:
        assert isinstance(item, dict), f"expected a dict but got a {type(item)}"
        assert item.get("label", None) is not None, f"field 'label' not found in  {item}"
        assert item.get("url", None) is not None, f"field 'url' not found in  {item}"


def check_occupation_dict(occupation_dict: dict) -> None:
    assert isinstance(occupation_dict, dict), f"expected a list but got a {type(occupation_dict)}"
    assert occupation_dict.get("label", None) is not None, f"field 'label' not found in  {occupation_dict}"
    assert occupation_dict.get("concept_id", None) is not None, f"field 'concept_id' not found in  {occupation_dict}"


def days_in_the_past(days):
    now = datetime.now()
    result = now - timedelta(days=days)
    return result.strftime("%Y-%m-%dT%H:%M:%S")


def list_of_ids_and_date(json_response):
    ids_and_dates = []
    for hit in json_response['hits']:
        tpl = (hit['id'], hit['publication_date'])
        ids_and_dates.append(tpl)
    return ids_and_dates


def verify_source_links(source_links):
    assert len(source_links) > 0, "no source links"
    assert isinstance(source_links, list), f"{source_links} was of type {type(source_links)}, expected a list"
    for item in source_links:
        display_name = item['label']
        assert display_name
        assert len(display_name) > 0, "displayName was empty"

        link = item['url']
        assert link
        assert len(link) > 0, "links was empty"
        assert 'http' in link
        assert len(item) == 2  # Only displayName and link


def _inject_source_links_test_data(es_result, faulty_source_links):
    """
    function to make it more obvious what's going on in the test
    """
    test_data = copy.deepcopy(es_result)
    test_data['source_links'] = faulty_source_links
    return test_data


def label_in_source_links(label, source_links):
    for link in source_links:
        if link['label'] == label:
            return True
    return False


def check_geo_concept_ids(wp_addresses, cities, region, country):
    assert len(wp_addresses) >= 1, "error, no workplace addresses"
    city_ok = False
    region_ok = False
    country_ok = False
    city = None
    for address in wp_addresses:
        city_ok = False
        region_ok = False
        country_ok = False

        city = address['municipality_concept_id']
        if city is not None:
            city_ok = city in cities
        if address['region_concept_id'] == region:
            region_ok = True
        if address['country_concept_id'] == country:
            country_ok = True
        if city_ok and region_ok and country_ok:
            break  # city, region and country are in at least one work place address

    assert city_ok, f"'{city} not found in {wp_addresses}"
    assert region_ok, f"'{region} not found in {wp_addresses}"
    assert country_ok, f"'{country} not found in {wp_addresses}"
    return True


def check_number_of_ads(json_response, expected):
    actual = json_response['total']['value']
    assert actual == expected, f"Got {actual} but expected {expected} hits"


def negative(text):
    # put - in from of text for negative search
    return f"-{text}"
