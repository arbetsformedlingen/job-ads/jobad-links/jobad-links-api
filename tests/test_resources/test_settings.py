import os

# tests depends on pipeline_2022-03-08_output.json with jae-synonym-dictionary-v-2.0.1.283

headers = {'accept': 'application/json'}

TEST_URL = os.getenv('TEST_URL', 'http://127.0.0.1:5000')
