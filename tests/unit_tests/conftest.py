import os

"""
This environment variable is set to True to prevent loading of synonym dictionary 
(which is not possible in a unit tests).
The default value (see joblinks/settings.py where environment variables are declared) is False.
"""
os.environ['DELAY_LOAD_SYNONYM_DICTIONARY_STARTUP'] = 'true'
