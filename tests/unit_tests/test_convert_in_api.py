import pytest
from joblinks.rest.endpoint.api import SearchJobLink


@pytest.mark.unit
def test_convert():
    search = SearchJobLink

    in_data = [{'id': '23d03b76908f3301e61b48c15b7e345f',
                'firstSeen': "2020-01-01",
                'workplace_addresses': [
                    {
                        "municipality_concept_id": "AvNB_uwa_6n6",
                        "municipality": "Stockholm",
                        "region_concept_id": "CifL_Rzy_Mku",
                        "region": "Stockholms län",
                        "country_concept_id": "i46j_HmG_v64",
                        "country": "Sverige"
                    },
                    {
                        "municipality_concept_id": "zHxw_uJZ_NJ8",
                        "municipality": "Solna",
                        "region_concept_id": "CifL_Rzy_Mku",
                        "region": "Stockholms län",
                        "country_concept_id": "i46j_HmG_v64",
                        "country": "Sverige"
                    }
                ],
                'occupation_group': {'label': 'Undersköterskor, hemtjänst, äldreboende m.fl.',
                                     'concept_id': 'jY19_knH_MJp'},
                'occupation_field': {'label': 'Hälso- och sjukvård', 'concept_id': 'NYW6_mP6_vwf'},
                'source_links': [
                    {'label': 'jobb',
                     'url': 'https://jobb.blocket.se/ledigt-jobb-i-borlange/DEKRA/Besiktningsingenj%C3%B6r-trycksatta-anordningar-Borl%C3%A4nge/1804971'},
                    {'label': 'jobbsafari',
                     'url': 'https://jobbsafari.se/jobbannons/besiktningsingenjor-trycksatta-anordningar-borlange/BJSS-1804971'}],
                'hashsum': 'AgnDv2Y=AMKIX8KtAMOTKRk=AGNYw60=AMKxKcK0ADPDmEM=AMOUwqoZAkjCqSE=+9b771f4d19d7cefd96f93bec845e120b',
                'originalJobPosting': {'title': 'Sjuksköterska',
                                       'brief': 'Barn- och ungdomspsykiatriska mottagningen, Länssjukhuset Ryhov, JönköpingVi sökerVi söker en sjuksköterska till vårt neuropsykiatriska team med ett intresse för den psykiatriska vården och den yngre målgruppen. Vi ser gärna att du har erfarenhet inom barnpsykiatri eller vuxenpsykiatri.',
                                       'url': 'https://arbetsformedlingen.se/platsbanken/annonser/24295766',
                                       'sameAs': '',
                                       'employer': {
                                           'name': 'Alfa Laval Lund AB'
                                       }},
                'keywords': {'enriched': {'occupation': ['sjuksköterska'],
                                          'skill': ['läkemedelsuppföljning',
                                                    'hälsa', 'barnpsykiatri',
                                                    'barn', 'vuxenpsykiatri'],
                                          'trait': ['positiv',
                                                    'samarbetsförmåga'],
                                          'location': ['sverige'],
                                          'compound': ['sjuksköterska',
                                                       'läkemedelsuppföljning',
                                                       'hälsa', 'barnpsykiatri',
                                                       'barn', 'vuxenpsykiatri',
                                                       'sverige']}},
                'relevance': 1.0}]

    expected = [{'id': '23d03b76908f3301e61b48c15b7e345f',
                 'headline': 'Sjuksköterska',
                 'brief': 'Barn- och ungdomspsykiatriska mottagningen, Länssjukhuset Ryhov, JönköpingVi sökerVi söker en sjuksköterska till vårt neuropsykiatriska team med ett intresse för den psykiatriska vården och den yngre målgruppen. Vi ser gärna att du har erfarenhet inom barnpsykiatri eller vuxenpsykiatri.',
                 'publication_date': '2020-01-01',
                 'workplace_addresses': [{'country': 'Sverige',
                                          'country_concept_id': 'i46j_HmG_v64',
                                          'municipality': 'Stockholm',
                                          'municipality_concept_id': 'AvNB_uwa_6n6',
                                          'region': 'Stockholms län',
                                          'region_concept_id': 'CifL_Rzy_Mku'},
                                         {'country': 'Sverige',
                                          'country_concept_id': 'i46j_HmG_v64',
                                          'municipality': 'Solna',
                                          'municipality_concept_id': 'zHxw_uJZ_NJ8',
                                          'region': 'Stockholms län',
                                          'region_concept_id': 'CifL_Rzy_Mku'}],
                 'occupation_field': {'label': 'Hälso- och sjukvård', 'concept_id': 'NYW6_mP6_vwf'},
                 'occupation_group': {'concept_id': 'jY19_knH_MJp',
                                      'label': 'Undersköterskor, hemtjänst, äldreboende '
                                               'm.fl.'},
                 'employer': {'name': 'Alfa Laval Lund AB'},
                 'source_links': [
                     {'label': 'jobb',
                      'url': 'https://jobb.blocket.se/ledigt-jobb-i-borlange/DEKRA/Besiktningsingenj%C3%B6r-trycksatta-anordningar-Borl%C3%A4nge/1804971'},
                     {'label': 'jobbsafari',
                      'url': 'https://jobbsafari.se/jobbannons/besiktningsingenjor-trycksatta-anordningar-borlange/BJSS-1804971'}]}]

    result = search.convert_hits(in_data)
    assert result == expected


@pytest.mark.unit
def test_convert_empty():
    search = SearchJobLink
    assert [] == search.convert_hits([])


@pytest.mark.unit
def test_convert_int_to_str():
    search = SearchJobLink
    expected = [{'brief': '',
                 'employer': '',
                 'headline': '',
                 'id': '99',
                 'occupation_field': {},
                 'occupation_group': {},
                 'publication_date': '2020-01-01',
                 'source_links': [],
                 'workplace_addresses': []}]
    result = search.convert_hits([{'id': 99, 'firstSeen': "2020-01-01"}])
    assert result == expected
    assert isinstance(result[0]['id'], str)
