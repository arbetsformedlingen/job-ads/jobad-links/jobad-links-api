import pytest
from joblinks.rest.endpoint.api import format_single_result
from tests.unit_tests.test_resources.test_data_format_es_result import es_result_test_data, expected_result


@pytest.mark.unit
def test_basic():
    result = format_single_result(es_result_test_data)
    assert result == expected_result


@pytest.mark.unit
def test_format_error():
    faulty_es_result = {'extra_key': None,
                        'id': 123,
                        'firstSeen': '2020-01-01'}
    result = format_single_result(faulty_es_result)
    assert result == {'id': '123', 'headline': '', 'brief': '', 'occupation_group': {},
                      'occupation_field': {}, 'employer': '', 'workplace_addresses': [], 'publication_date': '2020-01-01',
                      'source_links': []}


@pytest.mark.unit
def test_format_default_values():
    es_result = {'date_to_display_as_publish_date': '2020-01-01'}
    result = format_single_result(es_result)
    assert result == {'id': '', 'headline': '', 'brief': '', 'occupation_group': {},
                      'occupation_field': {}, 'employer': '', 'workplace_addresses': [], 'publication_date': '2020-01-01',
                      'source_links': []}


@pytest.mark.unit
@pytest.mark.parametrize("faulty_es_result", [None, [], set(), 1, 1.0, 'a', True, '\n'])
def test_format_attribute_error(faulty_es_result):
    with pytest.raises(AttributeError):
        format_single_result(faulty_es_result)
