import pytest
from joblinks.rest.endpoint.api import format_single_result
from tests.test_resources.helper import verify_source_links, _inject_source_links_test_data
from tests.unit_tests.test_resources.test_data_format_es_result import es_result_test_data


@pytest.mark.unit
def test_source_urls_format():
    """
    call format_single_result with test data and check results
    """
    result = format_single_result(es_result_test_data)
    source_urls = result['source_links']
    verify_source_links(source_urls)


@pytest.mark.unit
@pytest.mark.parametrize("source_urls_key_error", [
    [{"label_1": "wrong label name", "url": "http"}],
    [{"label": "hello", "url_url": "wrong label name"}],
])
def test_source_urls_key_error(source_urls_key_error):
    """
    call format_single_result with test data and check results
    modify ad sent to function with faulty source_urls and verify that a KeyError is raised.
    if no KeyError is raised, the test will fail
    """
    result_ok = format_single_result(es_result_test_data)
    verify_source_links(result_ok['source_links'])

    modified_es_result = _inject_source_links_test_data(es_result_test_data, source_urls_key_error)
    result = format_single_result(modified_es_result)
    with pytest.raises(KeyError):
        verify_source_links(result['source_urls'])


@pytest.mark.unit
@pytest.mark.parametrize("source_links_assertion_error", [
    [{"label": "ABCjob", "url": "https", "an_extra_parameter": "hello"}],
    [{"label": None, "url": "https"}],
    [{"label": "", "url": "https"}],
    [{"label": "my_label", "url": None}],
    [{"label": "my_label", "url": ""}],
    # []  TODO: include check for empty when source links are working in pipeline
])
def test_source_links_assertion_error(source_links_assertion_error):
    """
    call format_single_result with test data and check results
    modify ad sent to function with faulty source_urls and verify that an AssertionError is raised.
    if no AssertionError is raised, the test will fail
    """
    result_ok = format_single_result(es_result_test_data)
    verify_source_links(result_ok['source_links'])

    modified_es_result = _inject_source_links_test_data(es_result_test_data, source_links_assertion_error)
    result = format_single_result(modified_es_result)
    with pytest.raises(AssertionError):
        verify_source_links(result['source_links'])
