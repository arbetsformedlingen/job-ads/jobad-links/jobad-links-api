import pytest
from joblinks.rest.fetchfunction import transform_platsannons_query_result


@pytest.mark.unit
def test_transform():
    query_result = {'took': 1, 'timed_out': False, '_shards': {'total': 1, 'successful': 1, 'skipped': 0, 'failed': 0},
                    'hits': {'total': {'value': 2277, 'relation': 'eq'}, 'max_score': 46.77201, 'hits': [
                        {'_index': 'scrapedannons', '_type': '_doc', '_id': '23d03b76908f3301e61b48c15b7e345f',
                         '_score': 46.77201, '_source': {'id': '23d03b76908f3301e61b48c15b7e345f',
                                                         'workplace_address': {'municipality_concept_id': '',
                                                                               'municipality': '',
                                                                               'region_concept_id': '', 'region': '',
                                                                               'country_concept_id': 'i46j_HmG_v64',
                                                                               'country': 'Sverige'},
                                                         'occupation_group': {
                                                             'label': 'Undersköterskor, hemtjänst, äldreboende m.fl.',
                                                             'concept_id': 'jY19_knH_MJp'},
                                                         'occupation_field': {'label': 'Hälso- och sjukvård',
                                                                              'concept_id': 'NYW6_mP6_vwf'},
                                                         'hashsum': 'AgnDv2Y=AMKIX8KtAMOTKRk=AGNYw60=AMKxKcK0ADPDmEM=AMOUwqoZAkjCqSE=+9b771f4d19d7cefd96f93bec845e120b',
                                                         'originalJobPosting': {'title': 'Sjuksköterska',
                                                                                'brief': 'Barn- och ungdomspsykiatriska mottagningen, Länssjukhuset Ryhov, JönköpingVi sökerVi söker en sjuksköterska till vårt neuropsykiatriska team med ett intresse för den psykiatriska vården och den yngre målgruppen. Vi ser gärna att du har erfarenhet inom barnpsykiatri eller vuxenpsykiatri.',
                                                                                'url': 'https://arbetsformedlingen.se/platsbanken/annonser/24295766',
                                                                                'sameAs': ''}, 'keywords': {
                                'enriched': {'occupation': ['sjuksköterska'],
                                             'skill': ['läkemedelsuppföljning', 'hälsa', 'barnpsykiatri', 'barn',
                                                       'vuxenpsykiatri'], 'trait': ['positiv', 'samarbetsförmåga'],
                                             'location': ['sverige'],
                                             'compound': ['sjuksköterska', 'läkemedelsuppföljning', 'hälsa',
                                                          'barnpsykiatri', 'barn', 'vuxenpsykiatri', 'sverige']}}}}]}}

    expected = {'total': {'value': 2277, 'relation': 'eq'}, 'max_score': 46.77201, 'hits': [
        {'_index': 'scrapedannons', '_type': '_doc', '_id': '23d03b76908f3301e61b48c15b7e345f', '_score': 46.77201,
         '_source': {'id': '23d03b76908f3301e61b48c15b7e345f',
                     'workplace_address': {'municipality_concept_id': '', 'municipality': '', 'region_concept_id': '',
                                           'region': '', 'country_concept_id': 'i46j_HmG_v64', 'country': 'Sverige'},
                     'occupation_group': {'label': 'Undersköterskor, hemtjänst, äldreboende m.fl.',
                                          'concept_id': 'jY19_knH_MJp'},
                     'occupation_field': {'label': 'Hälso- och sjukvård', 'concept_id': 'NYW6_mP6_vwf'},
                     'hashsum': 'AgnDv2Y=AMKIX8KtAMOTKRk=AGNYw60=AMKxKcK0ADPDmEM=AMOUwqoZAkjCqSE=+9b771f4d19d7cefd96f93bec845e120b',
                     'originalJobPosting': {'title': 'Sjuksköterska',
                                            'brief': 'Barn- och ungdomspsykiatriska mottagningen, Länssjukhuset Ryhov, JönköpingVi sökerVi söker en sjuksköterska till vårt neuropsykiatriska team med ett intresse för den psykiatriska vården och den yngre målgruppen. Vi ser gärna att du har erfarenhet inom barnpsykiatri eller vuxenpsykiatri.',
                                            'url': 'https://arbetsformedlingen.se/platsbanken/annonser/24295766',
                                            'sameAs': ''}, 'keywords': {'enriched': {'occupation': ['sjuksköterska'],
                                                                                     'skill': ['läkemedelsuppföljning',
                                                                                               'hälsa', 'barnpsykiatri',
                                                                                               'barn',
                                                                                               'vuxenpsykiatri'],
                                                                                     'trait': ['positiv',
                                                                                               'samarbetsförmåga'],
                                                                                     'location': ['sverige'],
                                                                                     'compound': ['sjuksköterska',
                                                                                                  'läkemedelsuppföljning',
                                                                                                  'hälsa',
                                                                                                  'barnpsykiatri',
                                                                                                  'barn',
                                                                                                  'vuxenpsykiatri',
                                                                                                  'sverige'
                                                                                                  ]}}}}], 'took': 1,
                'concepts': {}}
    actual = transform_platsannons_query_result(query_result)
    assert expected == actual


@pytest.mark.unit
def test_transform_simple():
    query_result = {'took': 5, 'timed_out': False, '_shards': {'total': 1, 'successful': 1, 'skipped': 0, 'failed': 0},
                    'hits': {'total': {'value': 3333, 'relation': 'eq'}, 'max_score': 46.000, 'hits': []}}

    expected = {'total': {'value': 3333, 'relation': 'eq'}, 'max_score': 46.000, 'hits': [], 'took': 5, 'concepts': {}}
    actual = transform_platsannons_query_result(query_result)
    assert expected == actual
